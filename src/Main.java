import com.sun.scenario.effect.impl.sw.java.JSWColorAdjustPeer;
import javax.swing.*;
import java.util.*;
import java.lang.*;
import java.util.Scanner;
import java.math.*;
import java.util.regex.Pattern;

public class Main {
    int id;
    String name,address;
    double salary,bonus,rate,hourWorked;
    Scanner scan = new Scanner(System.in);

//    Add method
    public void addEmployee(ArrayList<StaffMember> arrList){
        System.out.print("1). Volunteer\t");
        System.out.print("2). Hourly Emp\t");
        System.out.print("3). Salaried Emp\t");
        System.out.print("4). Back\n\n");
        System.out.print("=> Choose option(1-4) : ");
        String choice = scan.nextLine();

        switch (choice){
            // Insert Volunteer
            case "1":
                System.out.println("====================== INSERT INFO ======================");
                    id = validateNum("=> Enter Staff Member's ID        : ");
                    System.out.print("=> Enter Staff Member's Name      : ");
                    name = scan.nextLine();
                    System.out.print("=> Enter Staff Member's Address   : ");
                    address = scan.nextLine();
//
//                Add to arrayList
                    arrList.add(new Volunteer(id,name,address));

//                Sort arrayList
                    Comparator <StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                    Collections.sort(arrList,sortName);

//                Output arrayList
                    Display(arrList);
                    break;

            // Insert Hour worked
            case "2":
                System.out.println("====================== INSERT INFO ======================");
                id = validateNum("=> Enter Staff Member's ID        : ");
                System.out.print("=> Enter Staff Member's Name      : ");
                name = scan.nextLine();
                System.out.print("=> Enter Staff Member's Address   : ");
                address = scan.nextLine();
                hourWorked = validateDou("=> Enter Staff Member's Hour Worked   : ");
                rate = validateDou("=> Enter Staff Member's rate : ");
                arrList.add(new HourlyEmployee(id,name,address,hourWorked,rate));
                Comparator <StaffMember> sortName1 = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                Collections.sort(arrList,sortName1);
                Display(arrList);
                break;

            // Insert Salary Emp
            case "3":
                System.out.println("====================== INSERT INFO ======================");
                id = validateNum("=> Enter Staff Member's ID        : ");
                System.out.print("=> Enter Staff Member's Name      : ");
                name = scan.nextLine();
                salary = validateDou("=> Enter Staff Member's Salary    : ");
                bonus = validateDou("=> Enter Staff Member's Bonus     : ");
                arrList.add(new SalariedEmployee(id,name,address,salary,bonus));
                Comparator <StaffMember> sortName2 = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
                Collections.sort(arrList,sortName2);
                Display(arrList);
                break;

            // Return back
            case "4":
                Menu(arrList);
                break;
            default:
                System.out.println("Please try again from (1-4)");
        }
    }

//    Display method
    public void Display(ArrayList<StaffMember> arrList){
        System.out.println("----------------------------------------");
        for (int a=0;a<arrList.size();a++){
            System.out.println(arrList.get(a));
            System.out.println("----------------------------------------");
        }
    }

//    Edit method
    public void Edit(ArrayList<StaffMember> arrList){
        System.out.println("====================== EDIT INFO ======================");
        int index = validateNum("=> Enter Employee ID to Update : ");
        for (StaffMember s : arrList){
            if (s.getId() == index){
                System.out.println(s);
                if (s instanceof Volunteer){
                    System.out.println("====================== NEW INFO OF STAFF MEMBER ======================");
                    System.out.print("=> Enter Staff Member's Name : ");
                    String editVName = scan.nextLine();
                    s.setName(editVName.substring(0,1).toUpperCase() + editVName.substring(1)); /*To make the first letter Uppercase */
                }else if (s instanceof SalariedEmployee){
                    System.out.println("====================== NEW INFO OF STAFF MEMBER ======================");
                    System.out.print("=> Enter Staff Member's Name : ");
                    String editSName = scan.nextLine();
                    Double editSalary = validateDou("=> Enter New Salary : ");
                    Double editBonus = validateDou("=> Enter New Bonus : ");
                    s.setName(editSName.substring(0,1).toUpperCase() + editSName.substring(1));
                    ((SalariedEmployee) s).setSalary(editSalary);
                    ((SalariedEmployee) s).setBonus(editBonus);
                }else if (s instanceof HourlyEmployee){
                    System.out.println("====================== NEW INFO OF STAFF MEMBER ======================");
                    System.out.print("=> Enter Staff Member's Name : ");
                    String editHName = scan.nextLine();
                    Double editHourworked = validateDou("=> Enter New Hourworked : ");
                    Double editRate = validateDou("=> Enter New Rate : ");
                    s.setName(editHName.substring(0,1).toUpperCase() + editHName.substring(1));
                    ((HourlyEmployee) s).setHourWorked(editHourworked);
                    ((HourlyEmployee) s).setRate(editRate);
                }
            }
        }
        Comparator <StaffMember> sortName = (StaffMember s1, StaffMember s2) -> (s1.getName().compareTo(s2.getName()));
        Collections.sort(arrList,sortName);
        Display(arrList);
    }

//    Remove method
    public void Remove(ArrayList<StaffMember> arrList){
        System.out.println("====================== DELETE ======================");
        int index = validateNum("=> Enter Employee ID to remove : ");
        // display Id arrayList
        for (StaffMember s : arrList){
            if (s.getId() == index){
                System.out.println(s);
            }
        }
        // remove ID arrayList
        Iterator<StaffMember> itr = arrList.iterator();
        while (itr.hasNext()){
            StaffMember num = itr.next();
            if (num.getId() == index){
                itr.remove();
            }
        }
        System.out.println("\n\tRemoved Successfully");
        Display(arrList);
    }
    // validateNumber
    public int validateNum(String n){
        Scanner scan = new Scanner(System.in);
        int number=0;
        while (true){
            System.out.print(n);
            String st = scan.nextLine();
            if (st.matches("[0-9]*")){
                number = Integer.parseInt(st);
                break;
            }else
                System.out.println("\t\tPLEASE INPUT NUMBER ONLY");
        }
        return number;
    }

    // validateDouble
    public double validateDou(String n){
        Scanner scan = new Scanner(System.in);
        Double number;
        while (true){
            System.out.print(n);
            String st = scan.nextLine();
            if (st.matches("\\d+(\\.\\d+)?")){
                number = Double.parseDouble(st);
                break;
            }else
                System.out.println("\t\tPLEASE INPUT NUMBER ONLY");
        }
        return number;
    }
//    Menu method
    public void Menu(ArrayList<StaffMember> arrList){
        do{
            Scanner scan = new Scanner(System.in);
            System.out.print("1). Add Employee\t");
            System.out.print("2). Edit\t");
            System.out.print("3). Remove\t");
            System.out.print("4). Exit\n\n");
            System.out.print("=> Choose option(1-4) : ");
            String choice = scan.nextLine();
            switch (choice){
                case "1":
                    System.out.println("----------------------------------------");
                    addEmployee(arrList);
                    break;
                case "2":
                    System.out.println("----------------------------------------");
                    Edit(arrList);
                    break;
                case "3":
                    Remove(arrList);
                    break;
                case "4":
                    System.out.println("\tGoodbye");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Please try again from (1-4)");
            }
        }while (true);
    }

    public static void main(String[] args){
//        Initialize arrayList
        ArrayList<StaffMember> arrList = new ArrayList<StaffMember>();
        StaffMember sa = new SalariedEmployee(1,"Both","Phnom Penh",300,20);
        StaffMember hr = new HourlyEmployee(2,"Dara","Phnom Penh", 60,15);
        StaffMember vo = new Volunteer(3,"Seyha","Kompong Cham");
        arrList.add(sa);
        arrList.add(hr);
        arrList.add(vo);
        Main obj = new Main();
        obj.Display(arrList);
        obj.Menu(arrList);
    }
}
