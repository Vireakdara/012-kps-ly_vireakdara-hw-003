public class  Volunteer extends StaffMember{
    // Constructor
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    // toString method
    @Override
    public String toString() {
        String st = "ID : "+ id + "\n" + "Name : "+ name + "\n"+ "Address : "+address;
        return st;
    }

    // pay() method
    @Override
    double pay() {
        return 0;
    }
}
