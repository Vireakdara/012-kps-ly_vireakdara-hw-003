abstract class StaffMember {
    // Variable
    protected int id;
    protected String name;
    protected String address;

    // Constructor
    public StaffMember(int id, String name, String address) {
        this.id = id;
        this.name = name.substring(0,1).toUpperCase() + name.substring(1);
        this.address = address;
    }

    // Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    // Method toString()
    @Override
    public String toString() {
        String st = "ID :"+ id + "\n" + "Name : "+ name + "\n"+ "Address :"+address;
        return st;
    }

    // abstract class
    abstract double pay();
}
