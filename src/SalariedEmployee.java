public class SalariedEmployee extends StaffMember{
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus(Double editBonus) {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        String st = "ID : "+ id + "\n" + "Name : "+ name + "\n"+ "Address : "+address+"\n"+"Salary : " + salary + "\n" + "Bonus : " + bonus + "\n"+ "Payment : " + pay();
        return st;
    }

    @Override
    double pay() {
        return salary+bonus;
    }
}
