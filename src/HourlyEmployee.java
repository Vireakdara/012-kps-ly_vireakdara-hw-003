public class HourlyEmployee extends StaffMember{
    private double hourWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, double hourWorked, double rate) {
        super(id, name, address);
        this.hourWorked = hourWorked;
        this.rate = rate;
    }

    public double getHourWorked() {
        return hourWorked;
    }

    public void setHourWorked(Double hourWorked) {
        this.hourWorked = hourWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        String st = "ID : "+ id + "\n" + "Name : "+ name + "\n"+ "Address : "+address+"\n"+"Hour Worked : " + hourWorked + "\n" + "Rate : " + rate +  "\n" +"Payment : "+ pay();
        return st;
    }

    @Override
    double pay() {
        return hourWorked*rate;
    }
}
